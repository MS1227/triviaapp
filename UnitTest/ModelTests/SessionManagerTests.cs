﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Trivia.Models;

namespace UnitTest.ModelTests
{
    [TestFixture]
    class SessionManagerTests
    {
        [Test]
        public void Session_Manager_Can_Get_And_Set_Total_Correct()
        {
            SessionManager test = new SessionManager();
            int temp = test.TotalCorrect;
            test.TotalCorrect++;

            Assert.AreNotEqual(temp, test.TotalCorrect);
        }
        [Test]
        public void Session_Manager_Can_Get_And_Set_Total_Incorrect()
        {
            SessionManager test = new SessionManager();
            int temp = test.TotalIncorrect;
            test.TotalIncorrect++;

            Assert.AreNotEqual(temp, test.TotalIncorrect);
        }
    }
}
