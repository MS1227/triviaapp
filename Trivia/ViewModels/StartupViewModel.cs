﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using RestSharp;
using Trivia.Models;

namespace Trivia.ViewModels
{
    public class StartupViewModel : BaseViewModel, IPageViewModel
    {

        private SessionManager m_sessionManager;
        public StartupViewModel()
        {
            m_sessionManager = new SessionManager();
            Categories = m_sessionManager.getCategories();
            m_selectedCategory = null;
            m_selectedDifficulty = null;

        }

        #region commands
        private ICommand m_buttonSubmit;
        public ICommand ButtonSubmit
        {
            get
            {
                return m_buttonSubmit ?? (m_buttonSubmit = new RelayCommand(x =>
                {
                    Game game = new Game( Categories[m_selectedCategory], m_selectedDifficulty);
                    Mediator.Notify("GoTo2Screen", game);
                },canExecute => CanSubmit()));
                
            }
        }
        #endregion

        #region properties
        public Dictionary<string, string> Categories { get; private set; }
        public string[] Difficulty { get; private set; } = { "easy", "medium", "hard" };

        private string m_selectedCategory;
        public string SelectedCategory
        { 
            get
            {
                return m_selectedCategory;
            }
            set
            {
                if (m_selectedCategory == value)
                    return;
                m_selectedCategory = value;
                
            }
        }
        private string m_selectedDifficulty;
        public string SelectedDifficulty
        {
            get
            {
                return m_selectedDifficulty;
            }
            set
            {
                if (m_selectedDifficulty == value)
                    return;
                m_selectedDifficulty = value;

            }
        }
        #endregion

                                                                      
        private bool CanSubmit()
        {
            return m_selectedDifficulty != null && m_selectedCategory != null;
        }
    }
    
}
