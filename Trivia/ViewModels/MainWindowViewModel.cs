﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trivia.Models;

namespace Trivia.ViewModels
{
        /*
         * Code courtesy of technical-recipes.com
         * This view model provides basic scaffolding for managing which view is loaded in the main window
         * 
         * */
        public class MainWindowViewModel : BaseViewModel
        {
            private IPageViewModel _currentPageViewModel;
            private List<IPageViewModel> _pageViewModels;

            private SessionManager m_sessionManager;
            public List<IPageViewModel> PageViewModels
            {
                get
                {
                    if (_pageViewModels == null)
                        _pageViewModels = new List<IPageViewModel>();

                    return _pageViewModels;
                }
            }

            public IPageViewModel CurrentPageViewModel
            {
                get
                {
                    return _currentPageViewModel;
                }
                set
                {
                    _currentPageViewModel = value;
                    OnPropertyChanged("CurrentPageViewModel");
                }
            }

            private void ChangeViewModel(IPageViewModel viewModel)
            {
                if (!PageViewModels.Contains(viewModel))
                    PageViewModels.Add(viewModel);

                CurrentPageViewModel = PageViewModels
                    .FirstOrDefault(vm => vm == viewModel);
            }

            private void OnGoPreferencesScreen(object obj)
            {
                ChangeViewModel(PageViewModels[0]);
            }

            private void OnGoGameScreen(object obj)
            {
                PageViewModels.Add(new GameViewModel((Game) obj));
                ChangeViewModel(PageViewModels[1]);
            }
            private void OnGoSummaryScreen(object obj)
            {
                Game temp = (Game)obj;
                m_sessionManager.TotalCorrect += temp.CorrectAnswers;
                m_sessionManager.TotalIncorrect += temp.IncorrectAnswers;

                PageViewModels.Add(new SummaryViewModel((Game) obj,m_sessionManager.TotalCorrect,m_sessionManager.TotalIncorrect));
                ChangeViewModel(PageViewModels[2]);
            }
            private void OnGoNewGame(object obj)
            {
                PageViewModels.Clear();
                PageViewModels.Add(new StartupViewModel());
                OnGoPreferencesScreen("");
            }

            public MainWindowViewModel()
            {
                PageViewModels.Add(new StartupViewModel());
                m_sessionManager = new SessionManager();

                CurrentPageViewModel = PageViewModels[0];

                Mediator.Subscribe("GoTo1Screen", OnGoPreferencesScreen);
                Mediator.Subscribe("GoTo2Screen", OnGoGameScreen);
                Mediator.Subscribe("GoToSummary", OnGoSummaryScreen);
                Mediator.Subscribe("GoToNewGame", OnGoNewGame);
            }
        }
    
}
