﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RestSharp;

namespace Trivia.Models
{
    /*
     * Class definition of a trivia game
     * 
     * */
    public class Game
    {
        //String constants for building an API request
        private const string TRIVIA_URI = "https://opentdb.com/api.php";
        private const string NUM_QUESTIONS = "5";
        private const string TYPE = "multiple";

        private string m_category;
        private string m_difficulty;
        
        
        public Game(string category, string difficulty)
        {
            m_category = category;
            m_difficulty = difficulty;
            questions = new List<Result>();
        }

        #region properties
        public int CorrectAnswers { get; set; }
        public int IncorrectAnswers
        {
            get
            {
                int.TryParse(NUM_QUESTIONS, out int totalQuestions);
                return totalQuestions - CorrectAnswers;
            }
        }

        public List<Result> questions { get; private set; }
        #endregion

        public bool isCorrectAnswer(string answer, int currentQuestion)
        {
            return answer == questions[currentQuestion].correct_answer;
        }

        public void getQuestions()
        {
            var client = new RestClient(TRIVIA_URI);
            var request = new RestRequest();

            request.AddParameter("amount", NUM_QUESTIONS);
            request.AddParameter("category", m_category);
            request.AddParameter("difficulty", m_difficulty);
            request.AddParameter("type", TYPE);

            try
            {
                var response = client.Execute<Questions>(request);
                if(response.Data.response_code != 0)
                {
                    switch(response.Data.response_code)
                    {
                        case 1:
                            throw new SystemException("No Results Could not return results. " +
                                "The API doesn't have enough questions for your query.");
                        case 2:
                            throw new SystemException("Invalid Parameter Contains an invalid parameter." +
                                " Arguements passed in aren't valid.");
                    }
                }
                questions = response.Data.results;

                decodeQuestions();
            }
            catch(Exception)
            {
                MessageBox.Show("No response from API. \nPlease ensure Internet connection is working and reopen");
            }

        }
        public string calculatePercent()
        {
            double.TryParse(NUM_QUESTIONS, out double convertedValue);
            double percentage = (CorrectAnswers / convertedValue) * 100;
   
            if(percentage % 5 == 0)
            {
                return percentage.ToString();
            }
            else
            {
                return percentage.ToString("n1");
            }
                
        }

        /*
         * This function unescapes escaped characters returned from the API
         * */
        private void decodeQuestions()
        {
            int i = 0;
            foreach (Result x in questions)
            {
                questions[i].correct_answer = System.Net.WebUtility.HtmlDecode(questions[i].correct_answer);
               
                questions[i].question = System.Net.WebUtility.HtmlDecode(questions[i].question);

                for ( int j = 0; j < questions[i].incorrect_answers.Count; j++)
                {
                    questions[i].incorrect_answers[j] = System.Net.WebUtility.HtmlDecode(questions[i].incorrect_answers[j]);
                }
                i++;
            }

        }
       
    }
}

