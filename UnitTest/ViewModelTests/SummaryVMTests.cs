﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Trivia.ViewModels;
using Trivia.Models;

namespace UnitTest.ViewModelTests
{
    [TestFixture]
    class SummaryVMTests
    {
        [Test]
        public void Can_Retrieve_Number_Of_Correct_Answers()
        {
            Game testGame = new Game("10", "easy");
            testGame.getQuestions();

            SummaryViewModel test = new SummaryViewModel(testGame,3,3);
            Assert.AreEqual(test.CorrectAnswers, 0);
        }
        [Test]
        public void Can_Retrieve_Number_Of_Incorrect_Answers()
        {
            Game testGame = new Game("10", "easy");
            testGame.getQuestions();

            SummaryViewModel test = new SummaryViewModel(testGame, 3, 3);
            Assert.AreEqual(test.IncorrectAnswers, 5);
        }
        [Test]
        public void Can_Retrieve_Number_Of_Total_Correct_Answers()
        {
            Game testGame = new Game("10", "easy");
            testGame.getQuestions();

            SummaryViewModel test = new SummaryViewModel(testGame, 3, 3);
            Assert.AreEqual(test.TotalCorrect, 3);
        }
        [Test]
        public void Can_Retrieve_Number_Of_Total_Incorrect_Answers()
        {
            Game testGame = new Game("10", "easy");
            testGame.getQuestions();

            SummaryViewModel test = new SummaryViewModel(testGame, 3, 3);
            Assert.AreEqual(test.TotalIncorrect, 3);
        }
        [Test]
        public void Can_Retrieve_Game_Percentage_Correct()
        {
            Game testGame = new Game("10", "easy");
            testGame.getQuestions();

            SummaryViewModel test = new SummaryViewModel(testGame, 3, 3);
            Assert.AreEqual(test.GamePercentage, "0");
        }
        [Test]
        public void Can_Retrieve_Total_Percentage_Correct()
        {
            Game testGame = new Game("10", "easy");
            testGame.getQuestions();

            SummaryViewModel test = new SummaryViewModel(testGame, 2, 3);
            Assert.AreEqual(test.TotalPercentage, "40");
        }

    }
}
