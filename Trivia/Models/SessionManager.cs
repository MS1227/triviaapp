﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trivia.ViewModels;
using RestSharp;
using System.Windows;

namespace Trivia.Models
{
    public class SessionManager
    {
        public int TotalCorrect { get; set; }
        public int TotalIncorrect { get; set; }
        private const string API_URI = "https://opentdb.com/";
        private const string CATEGORY_ENDPOINT = "api_category.php";
        private Dictionary<string,string> categories;

        public SessionManager()
        {
            categories = new Dictionary<string, string>();
            TotalCorrect = 0;
            TotalIncorrect = 0;
        }
        /*
         * Get categories from api using the RestSharp framework to simplify the data transaction.
         * The JSON response will automatically be deserialized into a list of TriviaCategories.
         * 
         * */
        public Dictionary<string,string> getCategories()
        {
            var client = new RestClient(API_URI);
            var request = new RestRequest(CATEGORY_ENDPOINT);
            try
            {
                var response = client.Execute<Categories>(request);

                foreach (TriviaCategory x in response.Data.trivia_categories)
                {
                    categories.Add(x.name.ToString(), x.id.ToString());
                }
            }
            catch (Exception)
            {

                MessageBox.Show("No response from API. \nPlease ensure Internet connection is working and reopen");

            }
            return categories;
        }
    }
}
