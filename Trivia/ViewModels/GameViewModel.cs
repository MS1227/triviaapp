﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Trivia.Models;

namespace Trivia.ViewModels
{
    public class GameViewModel : BaseViewModel, IPageViewModel
    {
        private Game m_currentGame;
        private bool buttonEnabled;
        


        public GameViewModel(Game game)
        {
            currChoices = new ObservableCollection<string>();
            m_currentGame = game;
            m_currentGame.getQuestions();


            QuestionNumber = 1;
            currIndex = 0;          
            buttonEnabled = false;

            radioButtonsEnabled = true;
            processQuestion();
        }

        #region Properties
        public string CurrentQuestion { get; set; }
        public int QuestionNumber { get; set; }
        public ObservableCollection<string> currChoices { get; set; }
        public int currIndex { get; private set; }
        public int CurrentIndex { get; set; }

       /* These properties are for binding the answer choices to the view.This is admittedly more rigid than desired and is a
        * result of mutiple attempts at a more true MVVM approach.Refactoring the application with an MVVM framework could eliminate
        * the need for these.
        * */
        public string idx0
        {
            get
            {
                return currChoices.ElementAt(0);
            }

        }
        public string idx1
        {
            get
            {
                return currChoices.ElementAt(1);
            }

        }
        public string idx2
        {
            get
            {
                return currChoices.ElementAt(2);
            }

        }
        public string idx3
        {
            get
            {
                return currChoices.ElementAt(3);
            }

        }
        public Brush idx0Color { get; set; } = Brushes.Black;
        public Brush idx1Color { get; set; } = Brushes.Black;
        public Brush idx2Color { get; set; } = Brushes.Black;
        public Brush idx3Color { get; set; } = Brushes.Black;

        private bool radioButtonsEnabled;
        public bool RadioButtonsEnabled
        {
            get
            {
                return radioButtonsEnabled;
            }
            set
            {
                if (radioButtonsEnabled == value)
                    return;
                radioButtonsEnabled = value;
                OnPropertyChanged("RadioButtonsEnabled");
            }
        }

        private string selectedAnswer;
        public string SelectedAnswer
        {
            get { return selectedAnswer; }
            set
            {
                if (selectedAnswer == value)
                    return;
                selectedAnswer = value;
                radioButtonsEnabled = false;

                {
                    switch (CurrentIndex)
                    {
                        case 0:
                            if (m_currentGame.isCorrectAnswer(idx0,currIndex))
                            {
                                idx0Color = Brushes.Green;
                                ++m_currentGame.CorrectAnswers;
                            }
                            else
                            {
                                idx0Color = Brushes.Red;
                                markCorrectAnswer();

                            }
                            buttonEnabled = true;
                            OnPropertyChanged("idx0Color");
                            break;
                        case 1:
                            if (m_currentGame.isCorrectAnswer(idx1, currIndex))
                            {
                                idx1Color = Brushes.Green;
                                ++m_currentGame.CorrectAnswers;
                            }
                            else
                            {
                                idx1Color = Brushes.Red;
                                markCorrectAnswer();

                            }
                            buttonEnabled = true;
                            OnPropertyChanged("idx1Color");
                            break;
                        case 2:
                            if (m_currentGame.isCorrectAnswer(idx2, currIndex))
                            {
                                idx2Color = Brushes.Green;
                                ++m_currentGame.CorrectAnswers;
                            }
                            else
                            {
                                idx2Color = Brushes.Red;
                                markCorrectAnswer();
                            }
                            buttonEnabled = true;
                            OnPropertyChanged("idx2Color");
                            break;

                        case 3:
                            if (m_currentGame.isCorrectAnswer(idx3, currIndex))
                            {
                                idx3Color = Brushes.Green;
                                ++m_currentGame.CorrectAnswers;
                            }
                            else
                            {
                                idx3Color = Brushes.Red;
                                markCorrectAnswer();
                            }

                            buttonEnabled = true;
                            OnPropertyChanged("idx3Color");
                            break;
                    }



                }


                OnPropertyChanged("RadioButtonsEnabled");
                OnPropertyChanged("SelectedAnswer");
            }
        }
        private int m_selectedIndex;
        public int SelectedIndex
        {
            get
            {
                return m_selectedIndex;
            }
            set
            {
                if (m_selectedIndex == value)
                    return;
                m_selectedIndex = value;
                OnPropertyChanged("SelectedIndex");
            }
        }
        #endregion

        #region Commands
        private bool canClickNext()
        {
            return buttonEnabled;
        }
        private ICommand m_buttonNext;
        public ICommand buttonNext
        {
            get
            {
                return m_buttonNext ?? (m_buttonNext = new RelayCommand(x =>
                {
                    if(currIndex < 4)
                    {
                        CurrentIndex = -1;
                        OnPropertyChanged("CurrentIndex");
                        buttonEnabled = false;
                        SelectedAnswer = null;
                        radioButtonsEnabled = true;
                        resetRadioForeground();
                        ++QuestionNumber;
                        OnPropertyChanged("QuestionNumber");
                        OnPropertyChanged("SelectedAnswer");
                        OnPropertyChanged("RadioButtonsEnabled");
                        currIndex++;
                        processQuestion();
                        
                    }
                    else
                    {
                        Mediator.Notify("GoToSummary", m_currentGame);
                    }
                },canExecute => canClickNext()));

            } 
        }
        #endregion
        
        private void processQuestion()
        {
            CurrentQuestion = m_currentGame.questions[currIndex].question;
            currChoices.Clear();
            getAndSortAnswers();
            OnPropertyChanged("currChoices");
            OnPropertyChanged("CurrentQuestion");

        }
        private void getAndSortAnswers()
        {
            List<string> tempList = new List<string>();

            //Taking advantage of the sort functionality of a List to mix up the order of the possible responses.
            foreach (string possibleAnswer in m_currentGame.questions[currIndex].incorrect_answers)
            {
                tempList.Add(possibleAnswer);
            }

            tempList.Add(m_currentGame.questions[currIndex].correct_answer);
            tempList.Sort();

            
            foreach (string sortedAnswer in tempList)
            {
                currChoices.Add(sortedAnswer);
            }
            OnPropertyChanged("idx0");
            OnPropertyChanged("idx1");
            OnPropertyChanged("idx2");
            OnPropertyChanged("idx3");

        }
        private void resetRadioForeground()
        {
            idx0Color = Brushes.Black;
            idx1Color = Brushes.Black;
            idx2Color = Brushes.Black;
            idx3Color = Brushes.Black;

            OnPropertyChanged("idx0Color");
            OnPropertyChanged("idx1Color");
            OnPropertyChanged("idx2Color");
            OnPropertyChanged("idx3Color");
        }
        private void markCorrectAnswer()
        {
            switch(currChoices.IndexOf(m_currentGame.questions[currIndex].correct_answer))
            {
                case 0:
                    idx0Color = Brushes.Green;
                    OnPropertyChanged("idx0Color");
                    break;
                case 1:
                    idx1Color = Brushes.Green;
                    OnPropertyChanged("idx1Color");
                    break;
                case 2:
                    idx2Color = Brushes.Green;
                    OnPropertyChanged("idx2Color");
                    break;
                case 3:
                    idx3Color = Brushes.Green;
                    OnPropertyChanged("idx3Color");
                    break;

            }

        }

    }
}
