﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Trivia.Models;

namespace Trivia.ViewModels
{
    public class SummaryViewModel : BaseViewModel, IPageViewModel
    {
        private Game currGame;
        public SummaryViewModel(Game game, int totalCorrect, int totalIncorrect)
        {
            currGame = game;
            m_totalCorrect = totalCorrect;
            m_totalIncorrect = totalIncorrect;

        }

        #region commands
        private ICommand m_buttonNewGame;
        public ICommand ButtonNewGame
        {
            get
            {
                return m_buttonNewGame ?? (m_buttonNewGame = new RelayCommand(x =>
                {
                    Mediator.Notify("GoToNewGame", "");
                }));

            }
        }
        #endregion

        #region properties

        private int m_totalIncorrect;
        public int TotalIncorrect
        {
            get
            {
                return m_totalIncorrect;
            }
        }
        private int m_totalCorrect;
        public int TotalCorrect
        {
            get
            {
                return m_totalCorrect;
            }
        }
        public int CorrectAnswers
        {
            get
            {
                return currGame.CorrectAnswers;
            }
        }
        public int IncorrectAnswers
        {
            get
            {
                return currGame.IncorrectAnswers;
            }
        }
        public string  GamePercentage
        {
            get
            {
                return currGame.calculatePercent();
            }
        }
        public string TotalPercentage
        {
            get
            {
                //This returns the value to one decimal place if not a whole number, making for a cleaner display
                double percentage = ((double)m_totalCorrect / (m_totalCorrect + m_totalIncorrect)) * 100;
                if (percentage % 5 == 0)
                {
                    return percentage.ToString();
                }
                else
                {
                    return percentage.ToString("n1");
                }

            }
        }
        #endregion

        
        
    }
}
