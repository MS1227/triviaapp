﻿using System;
using NUnit.Framework;
using Trivia.Models;

namespace UnitTests.ModelTests
{
    [TestFixture]
    public class GameTests
    {
        Game testGame = new Game("9", "easy");
        
        [Test]
        public void A_Game_Can_Retrieve_Questions()
        {
            testGame.getQuestions();
            Assert.IsNotEmpty(testGame.questions);
        }
        [Test]
        public void A_Game_Retrieves_Five_Questions()
        {
            testGame.getQuestions();
            Assert.AreEqual(testGame.questions.Count, 5);
        }
        [Test]
        public void A_Game_Question_Has_Three_Incorrect_Answers()
        {
            testGame.getQuestions();
            Assert.AreEqual(testGame.questions[0].incorrect_answers.Count, 3);
        }
       

    }
}
