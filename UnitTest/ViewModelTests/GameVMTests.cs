﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trivia.ViewModels;
using Trivia.Models;
using NUnit.Framework;

namespace UnitTest.ViewModelTests
{
    [TestFixture]
    class GameVMTests
    {
       
        [Test]
        public void Game_VM_Can_Retrieve_Question()
        {
            Game testGame = new Game("10", "easy");
            testGame.getQuestions();
            GameViewModel test = new GameViewModel(testGame);
            Assert.IsNotEmpty(test.CurrentQuestion);
        }
        [Test]
        public void Game_VM_Can_Retrieve_Answer_Choices()
        {
            Game testGame = new Game("10", "easy");
            testGame.getQuestions();
            GameViewModel test = new GameViewModel(testGame);
            Assert.IsNotNull(test.currChoices);
        }

       
    }
}
