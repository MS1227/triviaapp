﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trivia.ViewModels;
namespace UnitTest.ViewModelTests
{
    [TestFixture]
    class StartUpVMTests
    {
        
        [Test]
        public void Startup_Can_Retrieve_Available_Categories()
        {
            StartupViewModel test = new StartupViewModel();
            Assert.IsNotEmpty(test.Categories);
        }
        [Test]
        public void Startup_Init_Selected_Category_To_Null()
        {
            StartupViewModel test = new StartupViewModel();
            Assert.IsNull(test.SelectedCategory);
        }
        [Test]
        public void Startup_Init_Selected_Difficulty_To_Null()
        {
            StartupViewModel test = new StartupViewModel();
            Assert.IsNull(test.SelectedDifficulty);
        }
        [Test]
        public void Can_Assign_And_Retrieve_Category()
        {
            StartupViewModel test = new StartupViewModel();
            test.SelectedCategory = test.Categories["General Knowledge"];
            Assert.IsNotNull(test.SelectedCategory);
        }
        [Test]
        public void Can_Assign_And_Retrieve_Difficulty()
        {
            StartupViewModel test = new StartupViewModel();
            test.SelectedDifficulty = "easy";
            Assert.IsNotNull(test.SelectedDifficulty);
        }
    }
}
