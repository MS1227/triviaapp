﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivia.Models
{
    /*
     * This class outlines the schema of the JSON response for the questions returned from the api. RestSharp uses this to automatically
     * deserialize the JSON response.
     * */
    public class Result
    {
        public string category                { get; set; }
        public string type                    { get; set; }
        public string difficulty              { get; set; }
        public string question                { get; set; }
        public string correct_answer          { get; set; }
        public List<string> incorrect_answers { get; set; }
    }

    public class Questions 
    {       
        public int response_code { get; set; }
        public List<Result> results { get; set; }
    }


}
