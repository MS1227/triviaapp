﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivia.Models
{
    /*
     * This class outlines the schema of the JSON response for available categories from the api. RestSharp uses this to automatically
     * deserialize the JSON response.
     * */
    public class TriviaCategory
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class Categories
    {
        public List<TriviaCategory> trivia_categories { get; set; }
    }
}
